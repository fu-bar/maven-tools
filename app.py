import sys
from time import sleep

import fire

from maven_tools import MavenTools
from utils import log_helper
from utils.log_helper import get_logger

default_code_folder = '/Users/borismilner/workspace/code'

log = get_logger(logger_name='App')


def is_relevant_pom_file(file_name: str) -> bool:
    non_relevant = ['perso-parent', 'vanilla']
    for item in non_relevant:
        if item in pom.file_path.lower():
            return False
    return file_name.endswith('pom.xml')


def wait_until_logging_finishes():
    while not log_helper.logging_queue.empty():
        sleep(10)


if __name__ == '__main__':

    if len(sys.argv) > 1:
        fire.Fire(MavenTools)
    else:
        maven_tools = MavenTools()
        maven_tools.collect(base_folder=default_code_folder)

        for pom in maven_tools.get_pom_container().poms:
            if not is_relevant_pom_file(pom.file_path):
                continue
            if len(pom.dependency_management) < 20:
                log.warning("Check: {}".format(pom.file_path))

    wait_until_logging_finishes()
