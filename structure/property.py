from structure.gav import Gav


class Property:
    def __init__(self, key: str, value: str, source: Gav):
        self.key = key
        self.value = value
        self.source = source

    def __eq__(self, other):
        if isinstance(other, Property):
            return self.key == other.key and self.value == other.value

    def __hash__(self):
        return hash((self.key, self.value))
