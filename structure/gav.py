class Gav:
    def __init__(self, group_id: str, artifact_id: str, version: str):
        self.group_id = group_id
        self.artifact_id = artifact_id
        self.version = version

    def __eq__(self, other):
        return self.group_id == other.group_id \
               and self.artifact_id == other.artifact_id \
               and self.version == other.version

    def __hash__(self):
        return hash((self.group_id, self.artifact_id, self.version))

    def __str__(self):
        group_id = self.group_id if self.group_id else '((NO-GROUP-ID))'
        artifact_id = self.artifact_id if self.artifact_id else '((NO-ARTIFACT-ID))'
        version = self.version if self.version else '((NO-VERSION))'
        return group_id + '.' + artifact_id + '.' + version
