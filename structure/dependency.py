from benedict import BeneDict

from structure.gav import Gav


class Dependency:
    def __init__(self, benedict: BeneDict, source: Gav):
        self.source = source
        self.benedict = benedict
        self.gav = Gav(group_id=benedict.groupId, artifact_id=benedict.artifactId, version=benedict.version)

    def __eq__(self, other):
        if isinstance(other, Dependency):
            return self.gav == other.gav
        return False

    def __hash__(self):
        return hash(self.gav)

    def __str__(self):
        return str(self.gav)
