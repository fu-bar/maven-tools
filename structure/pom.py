from typing import Union, Set, Collection, List

import xmltodict
from benedict import BeneDict

from structure.dependency import Dependency
from structure.gav import Gav
from structure.property import Property
from utils.log_helper import get_logger

log = get_logger(logger_name='Pom-Structure')


class Pom:
    def __init__(self, file_path: str):
        self.file_path = file_path
        self.properties: Set[Property] = set()
        self.dependency_management: Set[Dependency] = set()

        with open(file_path, 'r') as pom_handle:
            self.benedict = BeneDict(xmltodict.parse(pom_handle.read()))

        self.gav: Gav = Gav(
            group_id=self.benedict.project.groupId if 'groupId' in self.benedict.project else None,
            artifact_id=self.benedict.project.artifactId,
            version=self.benedict.project.version if 'version' in self.benedict.project else None
        )

        self.add_properties([Property(key='project.version', value=self.gav.version, source=self.gav)])

        self.parent_gav: Union[Gav, None] = None
        if 'parent' in self.benedict.project:
            self.parent_gav = Gav(
                group_id=self.benedict.project.parent.groupId,
                artifact_id=self.benedict.project.parent.artifactId,
                version=self.benedict.project.parent.version
            )

        self.parent: Union[Pom, None] = None
        self.dependencies: Set[Dependency] = set()

        if 'properties' in self.benedict.project:
            self.collect_properties()

    def collect_properties(self):
        properties: BeneDict = self.benedict.project.properties

        for key, value in properties.items():
            self.properties.add(Property(key=key, value=value, source=self.gav))

    def add_properties(self, properties: Collection[Property]):
        self.properties.update(properties)

    def __eq__(self, other):
        if isinstance(other, Pom):
            return self.file_path == other.file_path
        return False

    def __hash__(self):
        return hash(self.file_path)

    def __str__(self):
        return str(self.gav)

    def resolve_transitive_properties(self):
        parent = self.parent
        while parent is not None:
            self.add_properties(parent.properties)
            parent = parent.parent

    def get_project_version_from_parent(self):
        parent = self.parent
        while parent is not None:
            if 'version' in parent.benedict.project.keys():
                self.benedict.project.version = parent.benedict.project.version
                return
            parent = parent.parent
        log.error('FAILED TO FIND PROJECT VERSION FROM PARENTS CHAIN {}'.format(self.gav))

    def could_be_property(self, key: str):
        if '${' not in key:
            return key  # Not a property

        key = key.replace('$', '')
        key = key.replace('{', '')
        key = key.replace('}', '')

        if key == 'project.version':
            if 'version' not in self.benedict.project:
                self.get_project_version_from_parent()
            return self.benedict.project.version

        for prop in self.properties:
            if prop.key == key:
                return prop.value

        return None

    def resolve_transitive_dependency_management(self, pom_container):
        parent = self.parent

        if 'dependencyManagement' not in self.benedict.project:
            if parent is not None:
                parent.resolve_transitive_dependency_management(pom_container=pom_container)
                self.dependency_management.update(parent.dependency_management)
            return

        # Has its own dependency-management

        managed_versions: Union[List[BeneDict], BeneDict] = self.benedict.project.dependencyManagement.dependencies.dependency

        if not isinstance(managed_versions, List):
            managed_versions = [managed_versions]

        for managed_version in managed_versions:
            version: str = managed_version.version

            managed_version.version = self.could_be_property(version)

            if self.__is_import_of_dependency_management(managed_version):

                imported_gav = Gav(group_id=managed_version.groupId,
                                   artifact_id=managed_version.artifactId,
                                   version=managed_version.version)

                if imported_gav.version is None:  # [OK] Should never happen
                    print(
                        'Failed with to find version of: ' + imported_gav.group_id + '.' + imported_gav.artifact_id)
                    continue

                imported_pom = pom_container.get_by_gav(imported_gav)

                if imported_pom is None:  # Seems to actually be missing - how is it imported?
                    continue

                imported_pom.resolve_transitive_dependency_management(pom_container=pom_container)
                self.dependency_management.update(imported_pom.dependency_management)
            else:
                dependency = Dependency(benedict=managed_version, source=self.gav)
                self.dependency_management.add(dependency)

    @staticmethod
    def __is_import_of_dependency_management(managed_version):
        return 'type' in managed_version.keys() \
               and 'scope' in managed_version.keys() \
               and managed_version.type == 'pom' \
               and managed_version.scope == 'import'
