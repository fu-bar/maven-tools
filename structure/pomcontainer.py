import os.path
import subprocess
from os.path import expanduser
from typing import Set, Collection, Union

from structure.gav import Gav
from structure.pom import Pom
from utils.log_helper import get_logger

log = get_logger(logger_name='PomContainer')


class PomContainer:
    def __init__(self):
        self.poms: Set[Pom] = set()
        self.m2_path = os.path.join(expanduser("~"), '.m2')

    def register(self, pom: Pom):
        if pom in self.poms:
            pass  # log a warning
        self.poms.add(pom)

    def get_by_gav(self, gav: Gav) -> Pom:
        for pom in self.poms:
            if pom.gav == gav:
                return pom
        return self.get_pom_from_m2(gav=gav)

    def get_pom_from_m2(self, gav: Gav) -> Pom:
        expected_pom_path = os.path.join(
            self.m2_path,
            'repository',
            os.path.join(*gav.group_id.split('.')),
            gav.artifact_id,
            gav.version,
            '-'.join((gav.artifact_id, gav.version)))
        expected_pom_path += '.pom'

        if not os.path.exists(expected_pom_path):
            command_parts = ["mvn", "dependency:get", "-DgroupId=" + gav.group_id, "-DartifactId=" + gav.artifact_id, "-Dversion=" + gav.version]
            log.debug("Running: {}".format(' '.join(command_parts)))
            subprocess.call(command_parts)

        if not os.path.exists(expected_pom_path):
            log.error('Can not find ==> {}'.format(expected_pom_path))
            return None

        resolved_pom = Pom(file_path=expected_pom_path)
        self.poms.add(resolved_pom)
        self.resolve_parents([resolved_pom])
        self.resolve_transitive_properties([resolved_pom])
        return resolved_pom

    def resolve_parents(self, poms: Union[Collection[Pom], None]):
        if poms is None:
            poms = self.poms
        for pom in set(poms):
            if pom.parent_gav is not None:
                parent_pom = self.get_by_gav(pom.parent_gav)
                pom.parent = parent_pom
                if pom.parent is not None and pom.gav.group_id is None:
                    pom.gav.group_id = parent_pom.gav.group_id

    def resolve_transitive_properties(self, poms: Union[Collection[Pom], None]):
        if poms is None:
            poms = self.poms
        for pom in poms:
            pom.resolve_transitive_properties()

    def resolve_dependency_management(self):
        for pom in set(self.poms):
            pom.resolve_transitive_dependency_management(self)

    def resolve_transitivity(self):
        self.resolve_parents(poms=None)
        self.resolve_transitive_properties(poms=None)
        self.resolve_dependency_management()
