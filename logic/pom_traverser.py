import os

from structure.pom import Pom


class PomTraverser:

    @staticmethod
    def traverse_poms(base_folder: str):
        for subdir, dirs, file_names in os.walk(base_folder):
            for file_name in file_names:
                if file_name == 'pom.xml':
                    pom_file_path = os.path.join(subdir, file_name)
                    yield Pom(pom_file_path)
