from typing import Union

from logic.pom_traverser import PomTraverser
from structure.pomcontainer import PomContainer
from utils.log_helper import get_logger

log = get_logger(logger_name='MavenTools')


class MavenTools:
    def __init__(self):
        self.pom_container: Union[PomContainer, None] = None
        self.base_folder = None

    def collect(self, base_folder: str):
        self.base_folder = base_folder
        self.pom_container = PomContainer()
        poms_generator = PomTraverser.traverse_poms(base_folder=self.base_folder)
        for pom in poms_generator:
            self.pom_container.register(pom)
        self.pom_container.resolve_transitivity()

    def get_pom_container(self) -> Union[PomContainer, None]:
        return self.pom_container
